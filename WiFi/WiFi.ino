#include <BearSSLHelpers.h>
#include <CertStoreBearSSL.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiClientSecureAxTLS.h>
#include <WiFiClientSecureBearSSL.h>
#include <WiFiServer.h>
#include <WiFiServerSecure.h>
#include <WiFiServerSecureAxTLS.h>
#include <WiFiServerSecureBearSSL.h>
#include <WiFiUdp.h>

//const char* ssid = "01ChonTam9-Tang 1";   //put your SSID of the router to connect with
//const char* pass = "chontam9@123";   //put your password of the router to connect with

//const char* ssid = "Nguyen Thi Ai Diem";
//const char* pass = "30818769";   

const char* ssid = "heyo";
const char* pass = "03041999"; 

int led;
String responce;
String req;

WiFiServer server(80);

void setup() 
{
  Serial.begin(115200);
  delay(10);
   
  Serial.print("Connecting to............");
  Serial.println(ssid);
  
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println(WiFi.localIP());
  Serial.println("");
  Serial.println("WiFi connected");
  
  server.begin();
  Serial.println("Server started.....");
  
  pinMode(16, OUTPUT);
  for(int i=1; i<4; i++)
  {
    pinMode(i, OUTPUT);
    digitalWrite(i, 1);
  }
}

void mode1() {
  while(1) {
   for(int i=1; i<4; i++)
    {
      digitalWrite(i, 0);
      delay(500);
      digitalWrite(i, 1);
      delay(500);
    }
  }
}

void mode2() {
  while(1) {
   for(int i=1; i<4; i++)
   {
      digitalWrite(i, 0);
      delay(500);
   }
   for(int i=1; i<4; i++)
   {
      digitalWrite(i, 1);
      delay(500);
   }
  }
}

void loop() 
{
  WiFiClient client = server.available();
  if (!client)
  {
    return;
  }
  Serial.println("new client");
  
  while(!client.available())
  {
    delay(1);
  }

  while(client) {
    req = client.readStringUntil('\n');
    for(int i=1; i<4; i++)
    {
      pinMode(i, OUTPUT);
      digitalWrite(i, 1);
    }
    Serial.println(req);
    client.flush();
  
    if (req.indexOf("on") != -1)
    {
      led = 1;
      responce = "ON";
    }
    else if (req.indexOf("off") != -1)
    {
      led = 0;
      responce = "OFF";
    }
    else if (req.indexOf("mode1") != -1)
    {
      mode1();
      responce = "mode1";
    }
    else if (req.indexOf("mode2") != -1)
    {
      mode2();
      responce = "mode2";
    }
    else 
    {
      Serial.println("invalid request");
      return;
    }

  digitalWrite(16, led);
  client.flush();
  client.print(responce);
  }
  
  delay(1);
  Serial.println("Client disconnected");
}
